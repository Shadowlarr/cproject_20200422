// ��������� ����������� ����� ����� ���������� ��������: ������, �� ������
// ����� ���� ������ ������ ��������� � �������� �����
// � ��������� ��� ������ �� �����, �� ������������� �����, �� ������������.

#include <iostream>

using namespace std;

bool PrintEven(bool j) // j - ������ ������, ��� 0 - ��� ������, 1 - �� ������
{
	char e = ' ';

	if (j) cout << "Print uneven numbers ? (y/n)" << '\n';
	else cout << "Print even numbers ? (y/n)" << '\n';
		
	cin >> e;
	switch (e) {
		case 'y':
			return 1;
			break;
		case 'n':
			return 0;
			break;
		default:
			PrintEven(j);
			break;
	};
}

void PrintNum(int Start, int Final, bool j) // j - ������ ������, ��� 0 - ��� ������, 1 - �� ������
{
	if (j)
	{
		cout << "Writing uneven numbers:" << '\n';
		Start = Start - (Start % 2) + 1; // ��������, ����� �������� ����� �������������� ���� �� ������
	}
	else
	{
		cout << "Writing even numbers:" << '\n';
		Start = Start + (Start % 2);	 // ��������, ����� �������� ����� �������������� ���� ������
	}
	

	for ( ; Start <= Final; Start = Start + 2)
		{
			cout << Start << '\n';
		}
}


int main()
{
	int Start = 0;
	int Final = 0;
	bool Even = 0;
	bool Uneven = 0;
	
	// �����, ����� ����� ���������� ��������
	Even = PrintEven(0); 
	Uneven = PrintEven(1);

	// �������� "����� ������ �� ���� ��������"
	if ((Even == 0)&&(Uneven == 0))
	{
		cout << "Goodbye!" << '\n';
		return 0;
	}
	
	cout << "Enter the starting number, inclusive:" << '\n';
	cin >> Start;
	cout << "Enter the final number, inclusive:" << '\n';
	cin >> Final;

	// �������� �� ��, ����� ����� ������
	if (Start > Final)
	{
		int Start1 = Final;
		Final = Start;
		Start = Start1;
		cout << "The starting number is greater than the final. I swap places." << '\n';
	}
	
	// ������� ����� �������� ���������� ����
	if (Even) PrintNum(Start, Final, 0);
	if (Uneven) PrintNum(Start, Final, 1);
	
	return 0;
}